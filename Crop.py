
import numpy as np
import os
import SimpleITK as sitk
from keras.models import load_model

from Model import config, dice_coefficient, dice_coefficient_loss

def load_old_model(model_file):
    print("Loading pre-trained model")
    custom_objects = {'dice_coefficient_loss': dice_coefficient_loss, 'dice_coefficient': dice_coefficient}
    try:
        from keras_contrib.layers import InstanceNormalization
        custom_objects["InstanceNormalization"] = InstanceNormalization
    except ImportError:
        pass
    try:
        return load_model(model_file, custom_objects=custom_objects)
    except ValueError as error:
        if 'InstanceNormalization' in str(error):
            raise ValueError(str(error) + "\n\nPlease install keras-contrib to use InstanceNormalization:\n"
                                          "'pip install git+https://www.github.com/keras-team/keras-contrib.git'")
        else:
            raise error

def get_mask(data):
    return ( np.array(data) >= 0.5 ).astype(int)

def get_prediction_from_array_with_reference(array, ref_img):
    img = sitk.GetImageFromArray(np.squeeze(array))

    resampler = sitk.ResampleImageFilter()
    resampler.SetDefaultPixelValue(0)
    resampler.SetOutputDirection(img.GetDirection())
    spacing = np.array(img.GetSize())*np.array(img.GetSpacing())/np.array(ref_img.GetSize())
    resampler.SetOutputSpacing( spacing )
    resampler.SetOutputOrigin( img.GetOrigin() )
    resampler.SetSize( ref_img.GetSize() )    
    out_img = resampler.Execute(img)

    out_array = sitk.GetArrayFromImage(out_img)
    out_array = get_mask(out_array)
    out_img = sitk.GetImageFromArray(out_array)

    out_img.CopyInformation(ref_img)  
    return out_img

def filter_mask_to_center(mask):
    connected = sitk.ConnectedComponent(mask)
    relabeled = sitk.RelabelComponent(connected, 1)
    np_mask = sitk.GetArrayFromImage(relabeled)
    coordinates = np.argwhere(np_mask==1)
    z_center = ( np.amax(coordinates[:,0]) + np.amin(coordinates[:,0]) ) / 2.0
    y_center = ( np.amax(coordinates[:,1]) + np.amin(coordinates[:,1]) ) / 2.0
    x_center = ( np.amax(coordinates[:,2]) + np.amin(coordinates[:,2]) ) / 2.0
    physical_center = np.array( (x_center, y_center, z_center) )    
    if mask.GetDirection() != (1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0) :
        print("Error of image direction")
    return physical_center

def crop_image(in_img, physical_center, size = config["image_shape"], if_mask = False):
    resampler = sitk.ResampleImageFilter()
    resampler.SetDefaultPixelValue(0)
    resampler.SetOutputDirection( in_img.GetDirection() )
    resampler.SetOutputSpacing( in_img.GetSpacing() )

    origin_original = np.array(in_img.GetOrigin())
    spacing_original = np.array(in_img.GetSpacing())

    coordinates = ( (physical_center - origin_original)/spacing_original - np.array(size)/2 ).astype(int)
    origin_fit = origin_original + coordinates*spacing_original
    
    origin = [0.0,0.0,0.0]
    for i in range(3):
        origin[i] = origin_fit[i]
    resampler.SetOutputOrigin( origin )

    size_int = [0,0,0]   
    for i in range(3):
        size_int[i] = int(size[i])
    resampler.SetSize( size )

    if if_mask:
        resampler.SetInterpolator(1)

    out_img = resampler.Execute(in_img)
    return out_img

def crop_results(folder, data_folder, imgs, subjects, model, overwrite=False):
    if not os.path.exists(folder) or overwrite:
        if not os.path.exists(folder):
            os.makedirs(folder)
    predictions = model.predict(imgs, batch_size=1, verbose=1)

    for i in range(len(imgs)):
        pred = predictions[i]
        sub = subjects[i]
        original_image = sitk.ReadImage(data_folder + "/" + sub + "/" + config["filename_image"])
        original_gt = sitk.ReadImage(data_folder + "/" + sub + "/" + config["filename_mask"])

        im_pred = get_prediction_from_array_with_reference(predictions[i], original_image)
        physical_center = filter_mask_to_center(im_pred)

        im_img = crop_image(original_image, physical_center, if_mask=0)
        im_gt = crop_image(original_gt, physical_center, if_mask=1)

        subject_folder = os.path.join(folder, sub)
        if not os.path.exists(subject_folder) or overwrite:
            if not os.path.exists(subject_folder):
                os.makedirs(subject_folder)
            sitk.WriteImage(im_img, subject_folder + config["filename_image"], True)
            sitk.WriteImage(im_gt, subject_folder + config["filename_mask"], True)

testing_imgs = np.load(config["validation_files"][0])
testing_subjects = list()
with open(config["validation_files"][2], 'r') as f:  
    for line in f:
        sub = line[:-1]
        testing_subjects.append(sub)
training_imgs = np.load(config["training_files"][0])
training_subjects = list()
with open(config["training_files"][2], 'r') as f:  
    for line in f:
        sub = line[:-1]
        training_subjects.append(sub)

model = load_old_model(config["model_file"])

crop_results("./cropped/", config["data_folder"], testing_imgs, testing_subjects, model)

crop_results("./cropped/", config["data_folder"], training_imgs, training_subjects, model)

